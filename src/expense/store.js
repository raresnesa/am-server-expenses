import dataStore from 'nedb-promise';

function validateExpense(expense) {
  if (!expense.item || expense.item.trim() === '')
    throw new Error('Invalid item name');
  if (!expense.price || expense.price < 0)
    throw new Error('Invalid item price');
}

export class ExpenseStore {
  constructor({ filename, autoload }) {
    this.store = dataStore({ filename, autoload });
  }

  async find(props) {
    if (props.item) {
      let regexp = new RegExp(props.item, 'i');
      return this.store.find({ ...props, item: regexp });
    }

    delete props.item;

    if (props._id) {
      return this.store.findOne(props);
    }

    return this.store.find(props);
  }

  async insert(expense) {
    validateExpense(expense);
    return this.store.insert({
      item: expense.item,
      price: expense.price,
      uid: expense.uid
    });
  }

  async update(props, expense) {
    validateExpense(expense);
    let dbExpense = await this.find({ _id: expense._id });
    if (!dbExpense) throw new Error('404');
    return this.store.update(props, expense);
  }

  async remove(props) {
    return this.store.remove(props);
  }
}

export default new ExpenseStore({
  filename: './db/expenses.json',
  autoload: true
});

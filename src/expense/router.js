import Router from 'koa-router';
import expenseStore from './store';
import { broadcast } from '../utils';

export const router = new Router();

router.get('/', async ctx => {
  const response = ctx.response;
  const item = ctx.query.item;
  const uid = ctx.state.user._id;
  const expenses = await expenseStore.find({ item, uid });
  response.body = expenses;
  response.status = 200;
});

router.get('/:id', async ctx => {
  const response = ctx.response;
  const _id = ctx.params.id;
  const uid = ctx.state.user._id;
  const expense = await expenseStore.find({ _id, uid });
  response.body = expense;
  response.status = 200;
});

router.post('/', async ctx => {
  const expense = ctx.request.body;
  const response = ctx.response;
  expense.uid = ctx.state.user._id;
  try {
    response.body = await expenseStore.insert(expense);
    response.status = 200;
    broadcast({ event: 'expense/created', payload: response.body });
  } catch (err) {
    response.body = [{ issue: err.message, type: 'Error' }];
    response.status = 400;
    console.log(err.message);
  }
});

router.put('/:id', async ctx => {
  const expense = ctx.request.body;
  expense.uid = ctx.state.user._id;
  const _id = ctx.params.id;
  const response = ctx.response;
  try {
    await expenseStore.update({ _id }, expense);
    response.body = expense;
    response.status = 200;
    broadcast({ event: 'expense/updated', payload: { ...expense, _id } });
  } catch (err) {
    response.body = [{ issue: err.message, type: 'Error' }];
    response.status = parseInt(err.message) || 400;
  }
});

router.del('/:id', async ctx => {
  const _id = ctx.params.id;
  await expenseStore.remove({ _id });
  ctx.response.status = 204;
  broadcast({ event: 'expense/deleted', payload: { _id } });
});

import { broadcast } from '../utils';

export const exceptionHandler = async (ctx, next) => {
  try {
    return await next();
  } catch (err) {
    ctx.response.body = { message: err.message || 'Unexpected error.' };
    ctx.response.status = err.status || 500;
  }
};

export const wsBroadcastExpenses = () => {
  let i = 1;
  setInterval(() => {
    const expense = {
      item: `Expense - ${i}`,
      price: i,
      _id: `${i}`
    };
    broadcast({ event: 'expense/created', payload: expense });
    i++;
  }, 7000);
};

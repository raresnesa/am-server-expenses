import WebSocket from 'ws';

let wss = null;
let sockets = [];

export const initWS = server => {
  wss = new WebSocket.Server({ server });
  wss.on('connection', ws => {
    ws.on('message', data => {
      sockets.push(ws);
    });
    ws.on('close', () => {});
  });
};

// Broadcast to all.
export const broadcast = data => {
  const string = JSON.stringify(data);
  sockets.forEach((client, key) => {
    if (client !== undefined && client.readyState === WebSocket.OPEN) {
      client.send(string);
    }
  });
};

import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import logger from 'koa-logger';
import cors from '@koa/cors';
import jwt from 'koa-jwt';

import { router as expenseRouter } from './expense';
import { router as authRouter } from './auth';
import {
  exceptionHandler,
  initWS,
  jwtConfig,
  wsBroadcastExpenses
} from './utils';

const app = new Koa();
const server = require('http').createServer(app.callback());
initWS(server);

app.use(exceptionHandler);
app.use(bodyParser());
app.use(cors());
app.use(logger());

const prefix = '/api';

// public
const publicApiRouter = new Router({ prefix });
publicApiRouter.use('/auth', authRouter.routes());
app.use(publicApiRouter.routes()).use(publicApiRouter.allowedMethods());

app.use(jwt(jwtConfig));

// protected by token
const apiRouter = new Router({ prefix });
apiRouter.use('/expense', expenseRouter.routes());
app.use(apiRouter.routes()).use(apiRouter.allowedMethods());

server.listen(3000);
console.log('Listening on port 3000');

// wsBroadcastExpenses();

import Router from 'koa-router';
import jwt from 'jsonwebtoken';
import userStore from './store';
import { jwtConfig } from '../utils';

export const router = new Router();

const createToken = user => {
  return jwt.sign(
    { username: user.username, _id: user._id },
    jwtConfig.secret,
    { expiresIn: '300 days' }
  );
};

router.post('/login', async ctx => {
  const credentials = ctx.request.body;
  const response = ctx.response;
  const user = await userStore.findOne(credentials);
  if (user) {
    response.body = { token: createToken(user) };
    response.status = 201;
  } else {
    response.body = { issue: [{ error: 'Invalid credentials' }] };
    response.status = 400;
  }
});

# Koa server

This is an app made for the mobile applications course at UBB.

**Nothing serious to see here :)**

![ceva gif](https://media1.tenor.com/images/1359ccb442c9ed7aa8eef1df01935591/tenor.gif?itemid=7551542)

## Running the app

```sh
git clone https://raresnesa@bitbucket.org/raresnesa/am-server.git
cd am-server
npm install
npm start
```

## Example requests

```sh
# Get all expenses
curl -v http://localhost:3000/api/expense

# Get one expense
curl -v http://localhost:3000/api/expense/67duUzpVw9IfQvOP

# Insert an expense
curl -d "{\"item\": \"My item\", \"price\": 10}"
     -H "Content-Type: application/json" http://localhost:3000/api/expense

# Update an expense
curl -d "{\"item\": \"My updated item\", \"price\": 25}"
     -H "Content-Type: application/json"
     -X PUT http://localhost:3000/api/expense/zlWN5HGaBaZR7RI1

# Delete an expense
curl -X DELETE http://localhost:3000/api/expense/zlWN5HGaBaZR7RI1
```
